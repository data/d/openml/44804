# OpenML dataset: airfoil_self_noise

https://www.openml.org/d/44804

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Data Description**

NASA data set, obtained from a series of aerodynamic and acoustic tests of two and three-dimensional airfoil blade sections conducted in an anechoic wind tunnel.

It comprises different size NACA 0012 airfoils at various wind tunnel speeds and angles of attack. The span of the airfoil and the observer position were the same in all of the experiments.

The task is to predict the (scaled) self noise.

**Attribute Description**

1. *frequency* - in Hertzs
2. *angle_of_attack* - in degrees
3. *chord_length* - in meters
4. *free_stream_velocity* - in meters per second
5. *displacement_thickness* - in meters
6. *sound_pressure* - in decibels (target feature)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44804) of an [OpenML dataset](https://www.openml.org/d/44804). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44804/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44804/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44804/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

